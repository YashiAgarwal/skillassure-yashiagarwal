import java.util.Scanner;
public class LargestNumber
{
	public static void main(String[] args) {
		//Step1: Declaration
		int num1, num2;
	    Scanner sc=new Scanner(System.in);
	    System.out.print("Enter number1 : ");
	    num1=sc.nextInt();
	    System.out.print("Enter number2 : ");
	    num2=sc.nextInt();
	    //step2: Logic
	    
	    if(num1>num2)
	    {
	        //step3: Display
	        System.out.println("Largest Number : "+ num1);
	    }
	    else{
	        System.out.println("Largest Number : "+ num2);
	    }
	
	}
}