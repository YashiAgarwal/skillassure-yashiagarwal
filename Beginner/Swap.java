//Swapping of two numbers

import java.util.Scanner;
public class Swap
{
public static void main(String[] args)
{

Scanner sc=new Scanner(System.in);
int num1,num2,i;
System.out.println("Enter two numbers : ");
num1=sc.nextInt();
num2=sc.nextInt();

System.out.println("Before Swapping " +num1 +num2);

i=num1;
num1=num2;
num2=i;

System.out.println("After Swapping " +num1 +num2);

}}
