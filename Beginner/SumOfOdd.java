// sum of all odd numbers.

import java.util.Scanner;
public class SumOfOdd
{
public static void main(String[] args)
{
Scanner sc= new Scanner(System.in);
int num,i,sum=0;

System.out.println("Enter the number:");
num=sc.nextInt();

for(i=1;i<=num;i++)
{
if(i%2!=0)
{
sum=sum+i;
}
else
{
continue;
}
}
System.out.println("Sum of Odd numbers is :" +sum);
}
}